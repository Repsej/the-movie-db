package com.ltu.m7019e.v23.themoviedb

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.ltu.m7019e.v23.themoviedb.database.MovieDetails
import com.ltu.m7019e.v23.themoviedb.databinding.FragmentMovieDetailBinding
import com.ltu.m7019e.v23.themoviedb.model.Movie
import com.ltu.m7019e.v23.themoviedb.model.MovieDetail

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class MovieDetailFragment : Fragment() {

    private var _binding: FragmentMovieDetailBinding? = null
    private val binding get() = _binding!!

    private lateinit var movie: Movie
    private lateinit var movieDetail: MovieDetail

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMovieDetailBinding.inflate(inflater)
        movie = MovieDetailFragmentArgs.fromBundle(requireArguments()).movie
        binding.movie = movie

        val movieDetails = MovieDetails()
        movieDetails.list.forEach {movieDetail ->
            if (movie.id == movieDetail.id) {
                this.movieDetail = movieDetail
                val fragmentMovieDetailBinding: com.ltu.m7019e.v23.themoviedb.databinding.FragmentMovieDetailBinding = androidx.databinding.DataBindingUtil.inflate(inflater, com.ltu.m7019e.v23.themoviedb.R.layout.fragment_movie_detail, container, false);
                fragmentMovieDetailBinding.movieDetail = movieDetail

                binding.movieDetail = movieDetail
            }
        }


        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.backToMovieList.setOnClickListener {
            findNavController().navigate(MovieDetailFragmentDirections.actionMovieDetailFragmentToMovieListFragment())
        }
        binding.toThirdFragment.setOnClickListener {
            findNavController().navigate(
                MovieDetailFragmentDirections.actionMovieDetailFragmentToReviewListFragment(
                    movie
                )
            )
        }

        binding.movieDetailImdbLink.setOnClickListener {
            OpenIMDB(movieDetail.imdb_link)
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    fun OpenIMDB(url: String){
        val parsedUrl = Uri.parse(url)
        val i = Intent(Intent.ACTION_VIEW, parsedUrl)
        try {
            startActivity(i)
        } catch (e: ActivityNotFoundException) {
            error("could not open url")
        }

    }
}