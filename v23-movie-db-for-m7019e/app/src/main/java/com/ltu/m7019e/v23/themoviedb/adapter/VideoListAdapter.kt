package com.ltu.m7019e.v23.themoviedb.adapter

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.VideoView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ltu.m7019e.v23.themoviedb.databinding.VideoListItemBinding
import com.ltu.m7019e.v23.themoviedb.model.Video

class VideoListAdapter() :  ListAdapter<Video, VideoListAdapter.ViewHolder>(VideoListDiffCallback()){
    class ViewHolder(private var binding: VideoListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        //private val mediaController: MutableMap<String, MediaController> = mutableMapOf()
        private val hasSetUrl: MutableMap<String, Boolean> = mutableMapOf()
        @SuppressLint("SetJavaScriptEnabled")
        fun bind(video: Video) {
            if (video.site == "YouTube") {
                binding.webView.settings.javaScriptEnabled = true
                binding.webView.settings.domStorageEnabled = true
                binding.webView.loadUrl("https://www.youtube.com/embed/" + video.key)
            }
            else {
                //TODO
                //OpenYoutube("https://www.youtube.com/embed/"+video.key, context)
            }

            binding.executePendingBindings()
        }

        @SuppressLint("QueryPermissionsNeeded")
        fun OpenYoutube(url: String, context: Context){
            val parsedUrl = Uri.parse(url)
            val i = Intent(Intent.ACTION_VIEW, parsedUrl)
            try {
                startActivity(context, i, null)
            } catch (e: ActivityNotFoundException) {
                error("could not open url")
            }
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = VideoListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class VideoListDiffCallback : DiffUtil.ItemCallback<Video>() {
    override fun areItemsTheSame(oldItem: Video, newItem: Video): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Video, newItem: Video): Boolean {
        return oldItem == newItem
    }

}