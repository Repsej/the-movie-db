package com.ltu.m7019e.v23.themoviedb.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Reviewer(
        var id: String,
        var name: String,
        var content: String,
        var createdAt: String
) : Parcelable