package com.ltu.m7019e.v23.themoviedb

import android.os.StrictMode
import com.ltu.m7019e.v23.themoviedb.utils.Secrets
import okhttp3.*
import org.json.JSONObject

class RestAPI {

    private val client = OkHttpClient()

    fun getDetails(movie_id: String): JSONObject {
        val urlBaseFirst = "https://api.themoviedb.org/3/movie/"
        val urlBaseSecond = "?api_key=" + Secrets.SecretKey + "&language=en-US"
        val url = urlBaseFirst+movie_id+urlBaseSecond
        return doCall(url)
    }

    fun getReviews(movie_id: String): JSONObject {
        val urlBaseFirst = "https://api.themoviedb.org/3/movie/"
        val urlBaseSecond = "/reviews?api_key=" + Secrets.SecretKey + "&language=en-US&page=1"
        val url = urlBaseFirst+movie_id+urlBaseSecond
        return doCall(url)
    }

    fun getVideos(movie_id: String): JSONObject {
        val urlBaseFirst = "https://api.themoviedb.org/3/movie/"
        val urlBaseSecond = "/videos?api_key=" + Secrets.SecretKey + "&language=en-US&page=1"
        val url = urlBaseFirst+movie_id+urlBaseSecond
        return doCall(url)
    }

    private fun doCall(url: String): JSONObject {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        val stringResponse = parseResponse(client.newCall(createRequest(url)).execute())
        val jsonResponse = JSONObject(stringResponse)
        return jsonResponse
    }

    private fun createRequest(url: String): Request {
        return Request.Builder()
            .url(url)
            .build()
    }

    private fun parseResponse(response: Response): String {
        return response.body()?.string() ?: ""
    }
}
