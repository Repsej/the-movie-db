package com.ltu.m7019e.v23.themoviedb

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ltu.m7019e.v23.themoviedb.adapter.MovieListAdapter
import com.ltu.m7019e.v23.themoviedb.adapter.MovieListClickListener
import com.ltu.m7019e.v23.themoviedb.adapter.ReviewListAdapter
import com.ltu.m7019e.v23.themoviedb.adapter.VideoListAdapter
import com.ltu.m7019e.v23.themoviedb.databinding.FragmentReviewListBinding
import com.ltu.m7019e.v23.themoviedb.model.Movie
import com.ltu.m7019e.v23.themoviedb.viewmodel.ReviewListViewModel
import com.ltu.m7019e.v23.themoviedb.viewmodel.ReviewListViewModelFactory

/**
 * A simple [Fragment] subclass as the third destination in the navigation.
 */
class ReviewListFragment : Fragment() {

    private lateinit var viewModel: ReviewListViewModel
    private lateinit var viewModelFactory: ReviewListViewModelFactory

    private var _binding: FragmentReviewListBinding? = null
    private val binding get() = _binding!!

    private lateinit var movie: Movie

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReviewListBinding.inflate(inflater)
        val application = requireNotNull(this.activity).application

        movie = ReviewListFragmentArgs.fromBundle(requireArguments()).movie

        viewModelFactory = ReviewListViewModelFactory(application, movie)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ReviewListViewModel::class.java)

        val reviewListAdapter = ReviewListAdapter()
        val videoListAdapter = VideoListAdapter()

        binding.reviewListRv.adapter = reviewListAdapter
        binding.videoListRv.adapter = videoListAdapter

        viewModel.reviewList.observe(
            viewLifecycleOwner
        ) { reviewList ->
            reviewList?.let {
                reviewListAdapter.submitList(reviewList)
            }
        }

        viewModel.videoList.observe(
            viewLifecycleOwner
        ) { videoList ->
            videoList?.let {
                videoListAdapter.submitList(videoList)
            }
        }

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.backToMovieDetails.setOnClickListener {
            findNavController().navigate(ReviewListFragmentDirections.actionReviewListFragmentToMovieDetailFragment(movie))
        }
    }
}