package com.ltu.m7019e.v23.themoviedb.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetail(
        var id: Int,
        var summery: String,
        var genre: String,
        var hyperlink: String,
        var imdb_link: String
) : Parcelable