package com.ltu.m7019e.v23.themoviedb.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ltu.m7019e.v23.themoviedb.databinding.ReviewListItemBinding
import com.ltu.m7019e.v23.themoviedb.model.Reviewer

class ReviewListAdapter() :  ListAdapter<Reviewer, ReviewListAdapter.ViewHolder>(ReviewListDiffCallback()){
    class ViewHolder(private var binding: ReviewListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(reviewer: Reviewer) {
            binding.reviewer = reviewer
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ReviewListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class ReviewListDiffCallback : DiffUtil.ItemCallback<Reviewer>() {
    override fun areItemsTheSame(oldItem: Reviewer, newItem: Reviewer): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Reviewer, newItem: Reviewer): Boolean {
        return oldItem == newItem
    }

}