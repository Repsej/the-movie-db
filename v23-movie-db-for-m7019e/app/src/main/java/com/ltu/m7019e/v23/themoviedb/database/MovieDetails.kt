package com.ltu.m7019e.v23.themoviedb.database
import com.ltu.m7019e.v23.themoviedb.model.MovieDetail

class MovieDetails {
    val list = mutableListOf<MovieDetail>()

    init {
        list.add(
            MovieDetail(
                527774,
                "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. But when an evil force threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, that same evil has returned and it’s up to a lone warrior, Raya, to track down the legendary last dragon to restore the fractured land and its divided people.",
                "Animation, Family, Fantasy, Action, Adventure",
                "https://www.themoviedb.org/movie/527774-raya-and-the-last-dragon",
                "https://www.imdb.com/title/tt5109280/"
            )
        )
        list.add(
            MovieDetail(
                793723,
                "Transferred home after a traumatizing combat mission, a highly trained French soldier uses her lethal skills to hunt down the man who hurt her sister.",
                "Thriller, Action, Drama",
                "https://www.themoviedb.org/movie/793723-sentinelle",
                "https://www.imdb.com/title/tt11734264/"
            )
        )
        list.add(
            MovieDetail(
                791373,
                "Determined to ensure Superman's ultimate sacrifice was not in vain, Bruce Wayne aligns forces with Diana Prince with plans to recruit a team of metahumans to protect the world from an approaching threat of catastrophic proportions.",
                "Action, Adventure, Fantasy, Science Fiction",
                "https://www.themoviedb.org/movie/791373-zack-snyder-s-justice-league",
                "https://www.imdb.com/title/tt12361974/"
            )
        )
        list.add(
            MovieDetail(
                587807,
                "Tom the cat and Jerry the mouse get kicked out of their home and relocate to a fancy New York hotel, where a scrappy employee named Kayla will lose her job if she can’t evict Jerry before a high-class wedding at the hotel. Her solution? Hiring Tom to get rid of the pesky mouse.",
                "Comedy, Family, Animation",
                "https://www.themoviedb.org/movie/587807-tom-jerry",
                "https://www.imdb.com/title/tt1361336/"
            )
        )
        list.add(
            MovieDetail(
                587996,
                "When a prisoner transfer van is attacked, the cop in charge must fight those inside and outside while dealing with a silent foe: the icy temperatures.",
                "Action, Crime, Thriller",
                "https://www.themoviedb.org/movie/587996-bajocero",
                "https://www.imdb.com/title/tt9845564/"
            )
        )
    }
}