package com.ltu.m7019e.v23.themoviedb.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Video(
        var id: String,
        var name: String,
        var site: String,
        var key: String
) : Parcelable