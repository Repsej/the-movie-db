package com.ltu.m7019e.v23.themoviedb.database

import com.ltu.m7019e.v23.themoviedb.model.Movie
import com.ltu.m7019e.v23.themoviedb.model.Reviewer

class Reviews {
    val list = mutableListOf<Reviewer>()
//    init {
//        list.add(
//            Reviewer(
//                1,
//                "AAAAAA",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. But when an evil force threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, that same evil has returned and it’s up to a lone warrior, Raya, to track down the legendary last dragon to restore the fractured land and its divided people.",
//                "Today"
//            )
//        )
//        list.add(
//            Reviewer(
//                2,
//                "B",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "This movie sucks!",
//                "Today"
//            )
//        )
//        list.add(
//            Reviewer(
//                3,
//                "C",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "This movie sucks!",
//                "Today"
//            )
//        )
//        list.add(
//            Reviewer(
//                4,
//                "D",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "This movie sucks!",
//                "Today"
//            )
//        )
//        list.add(
//            Reviewer(
//                5,
//                "E",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "This movie sucks!",
//                "Today"
//            )
//        )
//        list.add(
//            Reviewer(
//                6,
//                "F",
//                "/9xeEGUZjgiKlI69jwIOi0hjKUIk.jpg",
//                "This movie sucks!",
//                "Today"
//            )
//        )
//    }
}