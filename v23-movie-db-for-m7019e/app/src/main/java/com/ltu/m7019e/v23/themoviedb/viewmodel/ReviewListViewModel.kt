package com.ltu.m7019e.v23.themoviedb.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ltu.m7019e.v23.themoviedb.RestAPI
import com.ltu.m7019e.v23.themoviedb.database.Movies
import com.ltu.m7019e.v23.themoviedb.database.Reviews
import com.ltu.m7019e.v23.themoviedb.model.Movie
import com.ltu.m7019e.v23.themoviedb.model.Reviewer
import com.ltu.m7019e.v23.themoviedb.model.Video
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class ReviewListViewModel(application: Application, movie: Movie) : AndroidViewModel(application) {
    private val _reviewList = MutableLiveData<List<Reviewer>>()
    private val _videoList = MutableLiveData<List<Video>>()
    val api = RestAPI()
    val reviewList: LiveData<List<Reviewer>>
        get() {
            return _reviewList
        }
    val videoList: LiveData<List<Video>>
        get() {
            return _videoList
        }


    init {
        _reviewList.postValue(onLoadReviews(movie))
        _videoList.postValue(onLoadVideos(movie))
    }

    private fun onLoadReviews(movie: Movie): MutableList<Reviewer> {
        val reviews = mutableListOf<Reviewer>()
        val apiResponse = api.getReviews(movie.id.toString())
        //val videoTest = api.getVideos(movie.id.toString())
        val results: JSONArray = apiResponse.get("results") as JSONArray
        for(i in 0 until results.length()) {
            val review = results.getJSONObject(i)
            val id: String = review.get("id") as String
            val content: String = review.get("content") as String
            val createdAt: String = review.get("created_at") as String

            val authorDetails: JSONObject = review.get("author_details") as JSONObject
            val name: String = authorDetails.get("name") as String
            reviews.add(
                Reviewer(
                    id,
                    name,
                    content,
                    createdAt
                )
            )
        }
        return reviews
    }

    private fun onLoadVideos(movie: Movie): MutableList<Video> {
        val videos = mutableListOf<Video>()
        val apiResponse = api.getVideos(movie.id.toString())
        //val videoTest = api.getVideos(movie.id.toString())
        val results: JSONArray = apiResponse.get("results") as JSONArray
        for(i in 0 until results.length()) {
            val video = results.getJSONObject(i)
            val id: String = video.get("id") as String
            val name: String = video.get("name") as String
            val site: String = video.get("site") as String
            val key: String = video.get("key") as String

            videos.add(
                Video(
                    id,
                    name,
                    site,
                    key
                )
            )
        }
        return videos
    }

}